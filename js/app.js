// app.js
//angulasJS code for URL ROUTING

var routerApp = angular.module('eCommerce', ['ui.router']);

//ui routing
routerApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/pages/home');

    $stateProvider

        .state('home', {
            url: '/pages/home',
            templateUrl: 'pages/home.html'
        })

        .state('about', {
            url: '/pages/about',
            templateUrl: 'pages/about.html'
        })
        .state('products', {
            url: '/products/product',
            templateUrl: 'product.html'
        })

        .state('mail', {
            url: '/pages/mail',
            templateUrl: 'pages/mail.html'
        });
});

//http request
routerApp.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', '**']);
});
//setting URL constant to point to common directory
routerApp.constant('URL', '../cart/data/');

//service to get data from content.json
routerApp.factory('DataService', function($http, URL) {
    var getData = function() {
    return $http.get(URL + 'content.json');
    };
    return{
        getData : getData
        };
});

//service to get Template from templates.json
routerApp.factory('TemplateService', function($http, URL) {
    var getTemplates = function() {
    return $http.get(URL + 'templates.json');
    };
    return{
        getTemplates : getTemplates
        };
});

//controller to render data

routerApp.controller('myController', function (DataService) {
    var ctrl = this;

     ctrl.content = [];

     ctrl.fetchContent = function () {
        DataService.getData().then(function (result) {
          ctrl.content = result.data;
        });
      };
      ctrl.fetchContent();
});


//Custom directive   Passw0rDO0
routerApp.directive('contentItem',function ($compile, TemplateService) {
    var getTemplate = function (templates, contentType) {
        var template = '';

        switch (contentType) {
            case 'newstitle' :
                template = templates.newsTemplate;
                break;

            case 'footer' :
                             template = templates.footerTemplate;
                             break;

        }
            return template;

    };

    var linker = function (scope, element, attrs) {
        TemplateService.getTemplates().then(function (response) {
            var templates = response.data;
            element.html(getTemplate(templates, scope.content.content_type));
            $compile(element.contents()) (scope);
         });
    };

    return {
        restrict: 'E',
        link: linker,
        scope: {
            content: '='
         }
    };
    });

